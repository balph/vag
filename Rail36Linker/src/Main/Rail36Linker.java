/**
 * Created by Bas on 03/03/16.
 */

package Main;

import Rails3.UserInterface.MainUI;
import Rails6.RailsApp;

import javax.swing.*;

public class Rail36Linker {
    private static Rail36Linker instance = null;
    private MainUI rails3;
    private RailsApp rails6;

    public static void main(String[] args) {
        Rail36Linker.getInstance().initialize();
    }

    protected Rail36Linker() { }

    public static Rail36Linker getInstance() {
        if (instance == null) {
            instance = new Rail36Linker();
        }
        return instance;
    }

    public static MainUI getRail3() {
        return getInstance().rails3;
    }

    public static RailsApp getRail6() {
        return getInstance().rails6;
    }

    private void initialize() {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
        }

        String args[] = {};
        rails3 = new MainUI(args);
        rails3.setVisible(true);
        rails3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        rails6 = new RailsApp();
        rails6.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


}
