package Rails6.Tests;

import Rails6.Halte;
import Rails6.Reis;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Bas on 06/03/16.
 */
public class HalteTest {

    private Halte instance;

    Reis reis1;
    Reis reis2;
    Reis reis3;

    @Before
    public void setUp() throws Exception {
        instance = new Halte(1);
        reis1 = new Reis(2, instance, new Halte(1), new int[]{10,00});
        reis2 = new Reis(4, instance, new Halte(2), new int[]{11,00});
        reis3 = new Reis(6, instance, new Halte(6), new int[]{12,00});
    }

    @After
    public void tearDown() throws Exception {
        instance = null;
        reis1 = null;
        reis2 = null;
        reis3 = null;
    }

    @Test
    public void testGetId() throws Exception {
        int expResult = 1;
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetWisselOpen() throws Exception {
        instance.setWisselOpen(true);
        assertTrue(instance.getWisselOpen());
    }

    @Test
    public void testSetWisselOpen() throws Exception {
        instance.setWisselOpen(false);
        assertFalse(instance.getWisselOpen());
    }

    @Test
    public void testAddReis() throws Exception {
        instance.addReis(reis1);
        assertEquals(reis1, instance.getEersteReis());
    }

    @Test
    public void testGetEersteReis() throws Exception {
        instance.addReis(reis2);
        instance.addReis(reis3);
        assertEquals(reis3, instance.getEersteReis());
    }

    @Test
    public void testGetAantalWachtenden() throws Exception {
        instance.addReis(reis1);
        instance.addReis(reis2);
        instance.addReis(reis3);
        assertEquals(3, instance.getAantalWachtenden());
    }
}