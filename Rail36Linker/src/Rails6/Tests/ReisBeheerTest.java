package Rails6.Tests;

import Rails6.Halte;
import Rails6.Reis;
import Rails6.ReisBeheer;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Created by Bas on 06/03/16.
 */
public class ReisBeheerTest{

    private ReisBeheer instance;

    @Before
    public void setUp() throws Exception {
        instance = new ReisBeheer();
    }

    @After
    public void tearDown() throws Exception {
        instance = null;
    }

    @Test
    public void testGetWacht() throws Exception {
        Reis reis1 = new Reis(1, new Halte(5), new Halte(1), new int[]{00,00});
        Reis reis2 = new Reis(3, new Halte(5), new Halte(2), new int[]{00,00});
        Reis reis3 = new Reis(5, new Halte(5), new Halte(2), new int[]{00,00});
        instance.addReis(reis1);
        instance.addReis(reis2);
        instance.addReis(reis3);
        assertEquals(3, instance.getWacht(6));

    }

    @Test
    public void testAddReis() throws Exception {
        Reis reis = new Reis(1, new Halte(0), new Halte(1), new int[]{00,00});
        instance.addReis(reis);
        assertEquals(reis, instance.getReizen().get(instance.getReizen().size()-1));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetReizen() {
        //Test if returned List is immutable
        Reis reis = new Reis(2, new Halte(0), new Halte(1), new int[]{10, 00});
        instance.getReizen().add(reis);
    }

    @Test
    public void testSetHaltes() throws Exception {
        ArrayList<Halte> halteList = new ArrayList<Halte>() {{
            add(new Halte(1));
            add(new Halte(2));
            add(new Halte(3));
        }};

        instance.setHaltes(halteList);

        int i = 0;
        for (Halte halte : instance.getHaltes()) {
            assertEquals(halteList.get(i), halte);
            i++;
        }

    }

    @Test
    public void testGetAndAddHalte() throws Exception {
        instance.addHalte(0);
        instance.addHalte(1);
        assertEquals(1, instance.getHalte(1).getId());
    }

    @Test
    public void testGetActieveReizen() throws Exception {
        int[] tijd = {00,00};
        Reis reis1 = new Reis(10, new Halte(1), new Halte(2), tijd);
        Reis reis2 = new Reis(10, new Halte(3), new Halte(4), tijd);
        Reis reis3 = new Reis(10, new Halte(4), new Halte(5), tijd);
        Reis reis4 = new Reis(10, new Halte(5), new Halte(6), tijd);
        reis1.setStatus(Reis.Status.WACHTEND);
        reis2.setStatus(Reis.Status.REIZEND);
        instance.addReis(reis1);
        instance.addReis(reis2);
        instance.addReis(reis3);
        instance.addReis(reis4);

        for (Reis reis : instance.getActieveReizen()) {
            Assert.assertTrue(reis.getStatus() == Reis.Status.WACHTEND || reis.getStatus() == Reis.Status.REIZEND);

        }
    }
}