package Rails6;

import Main.Rail36Linker;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Cab extends ImageIcon {
	/**
	 * Cabs zijn objecten die in de gui worden weergegeven als plaatjes en een
	 * realistische weergave zijn van rijdende treinen waarmee ons algoritme
	 * moet werken.
	 */

    private static final long serialVersionUID = 1L;

	public boolean zichtbaar, inwissel = false, running = false;
	public int id, x, y;
	public Sensor sensor;
	public Reis reis;
	public int wtijd;
	public Timer timer;
	public int treinpositie;
	public byte status=0;
	public int bestemming;
	ReisBeheer rb;
	boolean rijdt;
	boolean inHalte;
	private int rxnr, rxoff;
	public double positie, angle;
	private double pl, rx, ry;
	TreinBeheer tb;
	public int tpositie =8;


	public Cab(int sid, ReisBeheer rb,TreinBeheer tb) {
		super("./cab.gif");
		this.tb=tb;
		id = sid;
		this.rb = rb;
		sensor = new Sensor(sid, this);
		setPositie(0);
		setZichtbaarheid(false);
		this.id=sid;

		this.tb=tb;
		timer =  new Timer(300,new Cab.TimerHandler());
		//timer.start();
		inHalte=false;
		rijdt=running;
		reis=null;

		//this.id=sid
		// TODO Auto-generated constructor stub
	}

	public void setZichtbaarheid(boolean z) {
		
		zichtbaar = z;
	}

	public void start() {
		running = true;
	}

	public void stop() {
		running = false;
	}

	public void run() {
		setPositie(positie + 0.05);
	}

	public void setPositie(double p) {
		positie = p;
		berekenPositieCab();
	}

	// Voer een reeks berekeningen uit om de positie
	// van het treintje op het scherm te bepalen
	private void berekenPositieCab() {
		// Zorgen dat positie altijd lager is als 100%
		if (positie > 100)
			positie = positie % 100;
		// Voortgang berekenen
		localiseer();

		// Bochten berekenen
		localiseer2();

		// Poll sensor
		psensor();

	}

	// Zoek de ware coordinaten uit
	private void localiseer() {
		if (positie >= 0 && positie < 37.5) {
			// Cab is boven
			rxoff = (int) (Math.round(rx) % 350);
			if (rxoff > 177 && rxoff < 183) {
				rxnr = (int) Math.round((rx - 180) / 350);
				checkWissel(rxnr);
			}
			wOffset(1);
			rx = 81 + ((1092 / 37.5) * positie);
			ry = 26 + pl;
		} else if (positie >= 37.5 && positie < 50) {
			// Cab is rechts
			if (ry > 117 && ry < 123) {
				checkWissel(3);
			}
			wOffset(2);
			rx = 1174 - pl;
			ry = 26 + ((379 / 12.5) * (positie - 37.5));
		} else if (positie >= 50 && positie < 87.5) {
			// Cab is onder
			rxoff = (int) (Math.round(rx) % 350);
			if (rxoff > 47 && rxoff < 53) {
				rxnr = (int) (7 - Math.round((rx - 180) / 350));
				checkWissel(rxnr);
			}
			wOffset(3);
			rx = 1174 - ((1092 / 37.5) * (positie - 50));
			ry = 405 + pl;
		} else if (positie >= 87.5 && positie < 100) {
			// Cab is links
			if (ry > 327 && ry < 333) {
				checkWissel(7);
			}
			wOffset(4);
			rx = 81 + pl;
			ry = 405 - ((379 / 12.5) * (positie - 87.5));
		}
	}

	// draai het plaatje in de bochten mbv ware coordinaten
	private void localiseer2() {
		int ux = (int) rx;
		int uy = (int) ry;
		if (ux < 151 && uy < 96) {
			// Linksboven
			angle = getAngle(ux - 151, uy - 96) + Math.PI;
			rx = 151 + Math.cos(angle) * 70;
			ry = 96 + Math.sin(angle) * 70;
		} else if (ux > 1103 && uy < 96) {
			// Rechtsboven
			angle = getAngle(ux - 1103, uy - 96) + (Math.PI * 2);
			rx = 1104 + Math.cos(angle) * 70;
			ry = 96 + Math.sin(angle) * 70;
		} else if (ux > 1103 && uy > 335) {
			// Rechtsonder
			angle = getAngle(ux - 1103, uy - 335);
			rx = 1104 + Math.cos(angle) * 70;
			ry = 335 + Math.sin(angle) * 70;
		} else if (ux < 151 && uy > 335) {
			// Linksonder
			angle = getAngle(ux - 151, uy - 335) + (Math.PI * 1);
			rx = 151 + Math.cos(angle) * 70;
			ry = 335 + Math.sin(angle) * 70;
		}

		x = (int) rx;
		y = (int) ry;
	}

	// Bereken de door openstaande wissels ontstane offset
	private void wOffset(int pos) {
		if (pos == 1) {
			// Cabs zijn boven
			if (!(rxoff >= 48 && rxoff < 178) && inwissel) {
				double rxoff2 = rx - (180 + (rxnr * 350));
				if (rxoff2 < 80.0) {
					pl = Math.sin(rxoff2 / 73) * 50;
				} else if (rxoff2 >= 80 && rxoff2 <= 125) {
					pl = 43;
				} else if (rxoff2 > 125 && rxoff2 < 200) {
					pl = Math.sin(1 - ((rxoff2 - 125) / 73)) * 50;
				} else {
					pl = 0;
				}

			} else {
				inwissel = false;
				pl = 0;
			}
		} else if (pos == 3) {
			// Cabs zijn onder
			if (!(rxoff >= 52 && rxoff < 178) && inwissel) {
				double rxoff2 = rx - (180 + ((6 - rxnr) * 350));
				if (rxoff2 > 0 && rxoff2 < 80.0) {
					pl = Math.sin(rxoff2 / 73) * 50;
				} else if (rxoff2 >= 80 && rxoff2 <= 125) {
					pl = 43;
				} else if (rxoff2 > 125 && rxoff2 < 200) {
					pl = Math.sin(1 - ((rxoff2 - 125) / 73)) * 50;
				} else {
					pl = 0;
				}
			} else {
				inwissel = false;
				pl = 0;
			}
		} else if (pos == 2 || pos == 4) {
			// Cabs zijn links of rechts
			if (inwissel) {
				if (ry >= 115 && ry <= 205) {
					pl = Math.sin((ry - 115) / 73) * 45;
				} else if (ry >= 205 && ry <= 250) {
					pl = 43;
				} else if (ry >= 250 && ry <= 320) {
					pl = Math.sin(1 - ((ry - 250) / 73)) * 45;
				} else {
					pl = 0;
				}
			} else {
				inwissel = false;
				pl = 0;
			}
		}
	}

	// Controleer of de trein een sensor passeert
	int sens[]={10,11,12,13,20,21,22,23,30,31,32,33,40,41,42,43,50,51,52,53,60,61,62,63,70,71,72,73,80,81,82,83};
	
	private void psensor() {
		int scheck = sensor.check();
		if (scheck > 0) {
			
			
			
			tb.sensorBericht(this.id,sens[scheck-1]);
			
			// hij is langs sensor scheck stuur een bericht naar treinbeheer
		}
	}

	// Controleer of de trein een station inrijd
	private void checkWissel(int wid) {
		if (rb.getHalte(wid).getWisselOpen()) {
			inwissel = true;
		} else {
			inwissel = false;
		}
	}

	// bereken hoek in graden
	private double getAngle(double com1, double com2) {
		// System.out.println(Math.cos(com1 / com2));
		return Math.atan(com2 / com1);
	}

	public int getId(){
        return id;
    }

	//////////////////algoritme/////////////////////////////
    public void sen0(Cab trein,int sid){
        //System.out.println("halte "+sid/10+" heeft "+reisBeheer.haltes.get((sid/10)-1).reizen.size()+" reizen");
        //als de halte de bestemming is of hij is vrij en er zijn wachtenden in halte:
        if(this.bestemming*10==sid||(tb.reisBeheer.getHalte((sid/10)-1).reizen.size()>0 && this.reis==null)){
            //test sector of die vrij is
            if(tb.sector[sid].trein==null){

                tb.reisBeheer.getHalte((sid/10)-1).setWisselOpen(true);
                this.treinpositie=sid;
                tb.sector[sid].trein=this;
                this.rij(true);
                //als het de eerste sensor is moet de laatste tb.sector vrijgemaakt worden
                if(sid==10&& tb.sector[84].trein==this){
                    tb.sector[84].trein=null;
                    //tb.sector[83].trein=null;
                }
                else if(tb.sector[sid-6].trein==this){
                    tb.sector[sid-6].trein=null;
                    //tb.sector[sid-7].trein=null;
                }

            }
            //als de sector niet vrij is stopt die
            else if(this.treinpositie!=sid){
                //System.out.println("trein "+this.id+" stopt omdat tb.sector "+sid+" is bezet  en sector nn "+this.positie+" is geblokkeert");
                this.rij(false);
                this.wacht();
            }
        }
        //als de halte niet de bestemming is:
        else{
            //getest of de parallelsector aan de halte vrij is
            if(tb.sector[sid+3].trein==null){
                this.rij(true);
                this.treinpositie=sid+3;
                tb.sector[sid+3].trein=this;
                tb.reisBeheer.getHalte((sid/10)-1).setWisselOpen(false);
                if(sid==10 && tb.sector[84].trein==this){
                    tb.sector[84].trein=null;

                }
                else if(tb.sector[sid-6].trein==this) {
                    tb.sector[sid-6].trein=null;

                }

            }

            else if(this.treinpositie!=sid+3){
                this.rij(false);
                this.wacht();
                //System.out.println("trein "+this.id+"stopt omdat sector "+(sid+3)+" is bezet  en sector ss "+this.positie+" is geblokkeert");
                }



        }
    }

	//als een this bij de uitstaphalte is:
    public void sen1(Cab trein,int sid){
        //System.out.println("sen1 methode");
        //heeft de trein een reis in zich
        if(this.reis!=null){
        //	System.out.println("er zit een reis in");

            this.rij(false);

            if(this.reis.getEchteBestemming().getId() != this.reis.getBestemming().getId()) {
                Rail36Linker.getRail3().nieuweDirecteReis(this.reis.getAantal(), this.reis.getEchteBestemming().getId() - 8);
            }

            this.status=0;

            this.reis.status= Reis.Status.BEEINDIGD;
            this.reis.stopReis();
            this.reis=null;
            this.bestemming=0;
     //   	System.out.println("trein  wacht");
            this.wacht();


        }
        else if(tb.sector[sid].trein==null){
            this.rij(true);
            this.treinpositie=sid;
            tb.sector[sid-1].trein=null;
            tb.sector[sid].trein=this;
        }
        else if(this.treinpositie!=sid){
            this.rij(false);
            this.wacht();
        //	System.out.println("trein "+this.id+"stopt omdat sector "+sid+" is bezet  en sector "+this.positie+" is geblokkeert");
            }







    }

	public void sen2(Cab trein, int sid){

        if(tb.reisBeheer.getHalte(((sid-2)/10)-1).reizen.size()>0 && this.reis==null ){
            //oppikken
            this.reis= tb.reisBeheer.getHalte(((sid-2)/10)-1).reizen.removeLast();
            this.reis.status= Reis.Status.REIZEND;
            this.rij(false);
            this.bestemming=this.reis.bestemmingHalte.id;
            //System.out.println("wactt"+this.inHalte);
            this.wacht();
        } else if(tb.sector[sid+1].trein==null &&tb.sector[sid].trein==null){
            this.rij(true);
            this.treinpositie=sid;
            tb.sector[sid-1].trein=null;
            tb.sector[sid].trein=this;
            tb.sector[sid+1].trein=this;
            //System.out.println("wactt"+this.inHalte);
        } else if(this.treinpositie!=sid){
            this.rij(false);
            this.wacht();
        //	System.out.println("trein "+this.id+"stopt omdat sector "+sid+" is bezet  en sector "+this.positie+" is geblokkeert");
        }
    }

	public void sen3(Cab trein, int sid){
        if(tb.sector[sid+1].trein==null){
            tb.sector[sid].trein=null;
            tb.sector[sid-1].trein=null;
            this.rij(true);
            tb.sector[sid+1].trein=this;
            this.treinpositie=sid+1;


        }
        // deze om te voorkomen dat bij eventueel snelle 2e aanroep de trein niet gestopt worden, want hij is net daarin gezet
        else if(this.treinpositie!=(sid+1) )
            {
            this.rij(false);
            //System.out.println("trein "+this.id+"stopt omdat sector "+(sid+1)+" is bezet  en sector "+this.positie+" is geblokkeert");
            this.wacht();
            }

    }

	public void wacht(){
        timer.start();
        wtijd=2;
        inHalte=true;
    }

	public Cab getThis(){
        return this;

    }

	public void addReis(Reis reis){
          this.reis= reis;
          this.bestemming= rb.getHaltes().indexOf(reis.getBestemming());
 //         this.bestemming.addFirst(bestemming);
      }

	public void rij(boolean r){
        //System.out.println("trein "+id+" rijdt "+r);
        if (r){
            start();
            rijdt=true;
        }
        else{
            stop();
            rijdt=false;
        }
    }

	public void setPos(int pos){
        treinpositie = pos;
    }

	public Reis getReis(){
        return reis;

    }

	public void laadUit(){

    }

	public void removeReis(){
        reis=null;
        status=0;
    }

	class TimerHandler implements ActionListener {


            public void actionPerformed(ActionEvent e){

                        if(wtijd>0){
                            wtijd--;
                        }
                        if((wtijd==0&&inHalte) && getThis().treinpositie%10==0){
                            inHalte=false;
         //   				System.out.println("bij de timer is de pos: "+positie+" trein "+id);
                            getThis().timer.stop();
                            sen1(getThis(),treinpositie+1);

                        }
                        else if((wtijd==0&&inHalte) && (getThis().treinpositie%10==1)){
                            inHalte=false;
                        //	System.out.println("bij de timer is de pos: "+positie+" trein "+id);
                            getThis().timer.stop();
                            sen2(getThis(),treinpositie+1);
                        }

                        else if(getThis().treinpositie%10==2){

                            getThis().timer.stop();
                            sen3(getThis(),treinpositie+1);
                        }
                        else if(getThis().treinpositie%10==3){

                            getThis().timer.stop();

                            sen3(getThis(),treinpositie);
                        }
                        else if(getThis().treinpositie%10==4){

                            getThis().timer.stop();
                            if(treinpositie==84){
                                sen0(getThis(),10);
                            }
                            else{
                                sen0(getThis(),treinpositie+6);
                            }
                        }
            }
        }
}