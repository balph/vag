package Rails6;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.swing.Timer;


public class Reis
{
///////////////////////////////////////////////////////ATRIBUTEN/////////////////////////////////////////////// 
   //private Calendar cal = new GregorianCalendar();
   private Timer timer;
   private int aantal;
   private Halte startHalte;
   public Halte bestemmingHalte;
   private Halte echteBestemming;
   private int[] tijd={99,99};
   public int id;
   
   public Status status;

   public enum Status {
       GERESERVEERD, WACHTEND, REIZEND, BEEINDIGD
   }

   //positie
   //private Object positie;
   
  private boolean gereserveerd;
   
  
   //in seconden
  public int wachttijd=0;
  public int reistijd=0;
   
 
   
   
    // tijd wordt ingevoer in de volgende formaat: {u,m} uren in 24fotmaat, dus niet hoger dan 23uur en 59minuten! anders loopt die vast
 
    public Reis(int aantal, Halte start, Halte eind, int[] tijd)    {

        if(eind.getId() > 7) {
            this.bestemmingHalte = ReisBeheer.getHalte(7);
            this.echteBestemming = eind;
        } else {
            bestemmingHalte = eind;
            echteBestemming = eind;
        }

        this.aantal=aantal;
        startHalte = start;
        bestemmingHalte = eind;
        this.tijd = tijd;
        System.out.println("nieuw reis aangemaakt\n"+aantal+" passagiers\n starthalte="+startHalte.getId()+
        "\n eindhalte="+ bestemmingHalte.getId()+"\n starttijd="+getStartTijd()); 
        timer =  new Timer(1000,new TimerHandler());
        gereserveerd=true;
        this.status = Status.GERESERVEERD;
        startReis();
        
    }
    
    public Reis( int aantal, Halte start, Halte eind){
        //this.id=id;
    	this.aantal=aantal;
        startHalte = start;
        bestemmingHalte = eind;
        status=Status.WACHTEND;
        
        timer =  new Timer(1000,new TimerHandler());
        startReis();
        gereserveerd= false;
        
    }
    
    
    //////////////////////////////////////////////////////////GETTERS///////////////////////////////////////////////////
    public String getStartTijd(){
        String t="";
        if(tijd[0]<10) t=t+"0";
        t=t+tijd[0]+":";
        if(tijd[1]<10) t=t+"0";
        t=t+tijd[1];
        return t;
    }
    public boolean gereserveerd(){
        return gereserveerd;
    }
    
    public int getAantal(){
    	return aantal;
    }
    
    public Status getStatus(){
        return status;
    }
    
    public Halte getStart(){
        return startHalte;
    }
    
     public Halte getBestemming(){
        return bestemmingHalte;
    }
   
    public int getWachtTijd(){
        return wachttijd;
    }
    
    public int getReistijd(){
        return reistijd;
    }
    
    public int getID(){
    	return id;
    }
    //////////////////////////////////////////////////////////SETTERS///////////////////////////////////////////////////
    
    
    public void setStatus(Status st){
        status=st;
    }
    
    
    void startReis(){
        
        timer.start();
        
    }
    
    void stopReis(){
       
        timer.stop();
        System.out.println("reistijd="+reistijd);
        System.out.println("wachttijd="+wachttijd);
        System.out.println("Reis beeindigt");
    }
    
    public Reis getThis(){
    	return this;
    }

    public Halte getEchteBestemming() {
        return echteBestemming;
    }
    //////////////////////////////////////////////////CONTROLE////////////////////////////////////////////////////////
    
                    //TIMERHANDLER//
                    //om de seconde voert die de in houd hiervan in
    class TimerHandler implements ActionListener{

        public void actionPerformed(ActionEvent e){
              Calendar c=new GregorianCalendar();
            int uur = RailsApp.uur; 
            int min = RailsApp.min;
             if(uur==(tijd[0]) && (min==(tijd[1]))) {
                if(status==Status.GERESERVEERD){
                	setStatus(Status.WACHTEND);
                	ReisBeheer.getHalte(startHalte.id - 1).addReis(getThis());
                }
            }
            //wachttijd en reistijd bijhouden
            if(status==Status.WACHTEND)
            wachttijd++;
            if(status==Status.REIZEND)
            reistijd++;
            if (status==Status.BEEINDIGD) stopReis();
            
        }
    
    
    
    }
}
    
        
  

