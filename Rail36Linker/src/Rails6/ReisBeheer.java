package Rails6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReisBeheer {
    private static List<Reis> reizen;
    private static ArrayList<Reis> treizen;
    private static List<Halte> haltes;
    int[] def = {99,99};
    TreinBeheer treinbeheer;
    

	public static int getWacht(int stat) {   //int station
		int wacht = 0;
			for (int i = 0; i < getReizen().size(); i++) {
			if ((getReizen().get(i).getStart().id) == (stat - 1)) {
				if (getReizen().get(i).getStatus() == Reis.Status.GERESERVEERD) {
					wacht++;
				}
			}
		}
		return wacht;
	}

    public ReisBeheer() {
        reizen = new ArrayList<Reis>();
        setTreizen(new ArrayList<Reis>());
        haltes = new ArrayList<Halte>();
        for(int i=1;i<17;i++){
            addHalte(i);
        }
 
   
        int[] tijd={99,99}; //add reizen voor test
        
        /*for(int i=0;i<8;i++){
        	addReis(5,(8-i),i+1,tijd);
        }
        */
    }
    
    public static void addReis(int aantal, int start, int bestemming, int[] tijd){
        Reis r;
       
        if(tijd[0]==99&&tijd[1]==99){
            r=new Reis(aantal, getHalte(start), getHalte(bestemming));
            addReis(r);
            getTreizen().add(r);
            getHalte(start).addReis(r);
            
        }
        else{
            r=new Reis(aantal, getHalte(start), getHalte(bestemming),tijd);
            addReis(r);
            getTreizen().add(r);
        }
        
       

        
    }
    public  static double gemReisttijd(){
    	int rt=0;
    	int aantal=0;
    	for(Reis r: getReizen()){
            if(r.status == Reis.Status.REIZEND) {
    			rt=rt+r.reistijd;
    			aantal++;
    		}
    	}
    	if(rt>0&&aantal>0)
    	return rt/aantal;
    	else return 0;
    
    	
    }
    
    public static double gemWachttijd(){
    	int wt=0;
    	int aantal=0;
    	for(Reis r: getReizen()){
            if(r.status == Reis.Status.WACHTEND || r.status == Reis.Status.REIZEND) {
    			wt=wt+r.wachttijd;
    			aantal++;
    		}
    	}
    	if(wt>0&&aantal>0){
    	return wt/aantal;
    	}else{
    		return 0;
    	}
    }

    // Fields encapsulated in commit cf76924ffd5adc23c952bf5f1b186d0ee2a67b0e

    public static List<Reis> getReizen() {
        return Collections.unmodifiableList(reizen);
    }

    public static void addReis(Reis reis) {
        reizen.add(reis);
    }

    public static void removeReis(Reis reis) {
        reizen.remove(reis);
    }

    public static void setReizen(ArrayList<Reis> reizen) {
        ReisBeheer.reizen = reizen;
    }

    public static ArrayList<Reis> getTreizen() {
        return treizen;
    }

    public static void setTreizen(ArrayList<Reis> treizen) {
        ReisBeheer.treizen = treizen;
    }

    public static List<Halte> getHaltes() {
        return Collections.unmodifiableList(haltes);
    }

    public static void setHaltes(ArrayList<Halte> haltes) {
        ReisBeheer.haltes = haltes;
    }

    public static Halte getHalte(int id){
        return getHaltes().get(id - 1);
    }
    
    
    public void addHalte(int id){
        Halte h=new Halte(id);
        haltes.add(h);
    }


    //reizen die wachten en in trein zitten
    public static ArrayList<Reis> getActieveReizen() {
        ArrayList<Reis> ar = new ArrayList<Reis>();
        for (Reis r : getReizen()) {
            if(r.getStatus() == Reis.Status.WACHTEND || r.getStatus() == Reis.Status.REIZEND)
                ar.add(r);
        }
        
        
        return ar;
    }
    

    
    //geeft alle reizen met deze status
    public static ArrayList<Reis> getReizen(Reis.Status status){
        ArrayList<Reis> ar=new ArrayList<Reis>();
        
        for(int i = 0; i< getReizen().size(); i++){
            if((getReizen().get(i)).getStatus()==status)
            ar.add(getReizen().get(i));
            
            
        }
       // System.out.println("reizen met status "+status+" zijn:"+ar.size());
        
        return ar;
    }
    //geeft de halte met de meeste wachtenden
    public Halte druksteHalte(){
        int aantal=0;
        Halte drukste =(Halte) getHalte(0);
        for(int i = 1; i< getHaltes().size(); i++){
            Halte h= getHalte(i);
            if(h.getAantalWachtenden()>aantal){
                drukste = h;
                aantal = h.getAantalWachtenden();
            }
        }
        if(aantal==0) return null;
        //System.out.println("halte nr "+drukste.getId());
        return drukste;
    }
    
	public Halte langsteWachttijdHalte() {
		Halte h, langsteWachttijdHalte = null;
		int wachttijd = 0;
		for (int i = 0; i < getHaltes().size(); i++) {
			h = getHalte(i);
			if (h.getEersteReis() != null)
				if (h.getEersteReis().getWachtTijd() > wachttijd) {
					wachttijd = h.getEersteReis().getWachtTijd();
					langsteWachttijdHalte = h;
				}
		}

		if (langsteWachttijdHalte != null)
			System.out.println("halte nr " + langsteWachttijdHalte.getId());
		return langsteWachttijdHalte;
	}

          
}
